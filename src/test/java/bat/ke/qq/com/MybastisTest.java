package bat.ke.qq.com;/*
 * 　　　┏┓　　　┏┓
 * 　　┏┛┻━━━┛┻┓
 * 　　┃　　　━　　　┃
 * 　　┃　┳┛　┗┳　┃
 * 　　┃　　　┻　　　┃
 * 　　┗━┓　　　┏━┛
 * 　　　　┃　　　┗━━━┓
 * 　　　　┃　　永无BUG 　┣┓
 * 　　　　┃　　如来保佑　┏┛
 * 　　　　┗┓┓┏━┳┓┏┛
 * 　　　　　┗┻┛　┗┻┛
 * 源码学院-只为培养BAT程序员而生
 * bat.ke.qq.com
 * Monkey老师
 */



import bat.ke.qq.com.mapper.UserMapper;
import bat.ke.qq.com.pojo.User;
import bat.ke.qq.com.session.Configuration;
import bat.ke.qq.com.session.DefaultSqlSession;
import bat.ke.qq.com.session.SqlSessionFactory;
import bat.ke.qq.com.session.SqlSessionFactoryBuilder;


import java.io.IOException;
import java.io.InputStream;

public class MybastisTest {

  public static void main(String[] args) throws IOException {
    InputStream inputStream = MybastisTest.class.getClassLoader().getResourceAsStream("mybatis-config.xml");
    Configuration configuration = new Configuration();
    configuration.setInputStream(inputStream);
    SqlSessionFactory sqlSessionFactory = new SqlSessionFactoryBuilder().build(configuration);
    DefaultSqlSession sqlSession = (DefaultSqlSession) sqlSessionFactory.openSession(configuration);
    UserMapper userMapper=sqlSession.getMapper(UserMapper.class);
    User user = userMapper.getUser(1);
    System.out.println(user);


  }
}
